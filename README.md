# About
This App displays the latest 20 photos with tag '#bskyb' from Flickr in a custom built Carousel

## Description
The App is composed of 2 main components:
- Header: which shows the Sky logo
- Carousel: which renders the Carousel which downloads the images from Flickr

The App uses Webpack to compile js/css into a bundle.js that it then served using an Express server on the route '/'.

## How to run the App
```
yarn install
yarn start
or
npm install
npm start
```

### Running tests
```
yarn test
```
