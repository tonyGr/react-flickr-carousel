import React, { Component } from 'react';
import { render } from 'react-dom';
import Header from './components/Header';
import Carousel from './components/Carousel';
import './style/main.css';

class App extends Component {
  render () {
    return (
      <div>
        <Header />
        <Carousel />
      </div>
    );
  }
}

render(<App/>, document.getElementById('app'));
