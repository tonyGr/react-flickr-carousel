import React from 'react';
import { shallow } from 'enzyme';
import Carousel from '../Carousel';
import { SIGSTOP } from 'constants';

let component;

beforeEach(() => {
  component = shallow(<Carousel />);
})

describe('Carousel', () => {

  it('should not crash', () => {
    expect(() => shallow(<Carousel />)).not.toThrow();
  });

  describe('photo index', () => {
    it('current photo index should be -1 when App launches', () => {
      const currentIndex = component.instance().state.currentIndex;
      expect(currentIndex).toEqual(-1);
    });
  });

  describe('getCurrentPhoto', () => {
    it('should return undefined when the App launches and we have no id', () => {
      expect(component.instance().getCurrentPhoto()).toEqual(undefined);
    });

    it('should return the correct index when we change the image', () => {
      const mockedState = {
        currentIndex: 0,
        photos: [
          { photo: 'test' }
        ]
      };

      component.instance().setState({
        ...mockedState
      });

      expect(component.instance().getCurrentPhoto()).toEqual(mockedState.photos[mockedState.currentIndex]);
    });
  });

  describe('nextPhoto', () => {
    it('should increase photo index by 1 when going to the next slide', () => {
      const mockedState = {
        currentIndex: 1,
        photos: [
          { photo: 'test' },
          { photo: 'test2' },
          { photo: 'test3' },
        ]
      };
      component.instance().setState({ ...mockedState });
      component.instance().nextPhoto();
      expect(component.instance().state.currentIndex).toEqual(2);
    });

    it('should set photo index to 0 if photo index is equal to the last', () => {
      const mockedState = {
        currentIndex: 2,
        photos: [
          { photo: 'test' },
          { photo: 'test2' },
          { photo: 'test3' },
        ]
      };
      component.instance().setState({ ...mockedState });
      component.instance().nextPhoto();
      expect(component.instance().state.currentIndex).toEqual(0);
    });

  });

  describe('previousPhoto', () => {

    it('should decrease photo index by 1 when going to the previous slide', () => {
      const mockedState = {
        currentIndex: 1,
        photos: [
          { photo: 'test' },
          { photo: 'test2'},
          { photo: 'test3'},
        ]
      };
      component.setState({ ...mockedState });
      component.instance().previousPhoto();
      expect(component.instance().state.currentIndex).toEqual(0);
    });
    it('should set photo index to last if photo index is equal to 0', () => {
      const mockedState = {
        currentIndex: 0,
        photos: [
          { photo: 'test' },
          { photo: 'test2' },
          { photo: 'test3' },
        ]
      };
      component.instance().setState({ ...mockedState });
      component.instance().previousPhoto();
      expect(component.instance().state.currentIndex).toEqual(2);
    });

  });

  describe('renderCurrentPhoto', () => {
    it('should return undefined if we dont have any photo', () => {
      expect(component.instance().renderCurrentPhoto()).toEqual(undefined)
    });

    it('should return the current photo if it is available', () => {
      const mockedState = {
        currentIndex: 1,
        photos: [
          { photo: 'test' },
          { photo: 'test2' },
          { photo: 'test3' },
        ]
      };
      component.instance().setState({  ...mockedState });
      expect(component.instance().renderCurrentPhoto()).toEqual(<img className="image" data-test-id="carousel-image-1" />);
    });
  });
});
