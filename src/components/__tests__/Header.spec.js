import React from 'react';
import { shallow } from 'enzyme';
import Header from '../Header';

let component;

beforeEach(() => {
  component = shallow(<Header />);
})

describe('Header', () => {
  it('should not crash', () => {
    expect(() => shallow(<Header />)).not.toThrow();
  });

  it('should render a header component', () => {
    expect(component.find({ 'data-test-id': 'header' })).toHaveLength(1);
  });

  it('should render a logo', () => {
    expect(component.find({ 'data-test-id': 'header-logo' })).toHaveLength(1);
  });
});
