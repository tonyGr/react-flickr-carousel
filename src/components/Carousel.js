import React, { Component } from 'react';
import Flickr from 'flickr-sdk';
import flickrConf from '../conf/flickr.json';

export default class Carousel extends Component {

  constructor(props) {
    super(props);

    this.state = {
      photos: [],
      currentIndex: -1
    };

    this.nextPhoto = this.nextPhoto.bind(this);
    this.previousPhoto = this.previousPhoto.bind(this);
  }

  componentDidMount() {
    this.loadPhotos();
  }

  nextPhoto() {
    if (this.state.currentIndex === this.state.photos.length - 1)
      return this.setState({ currentIndex: 0 });

    return this.setState(prevState => ({ currentIndex: prevState.currentIndex + 1 }));
  }

  previousPhoto() {
    if (this.state.currentIndex === 0)
      return this.setState({ currentIndex: this.state.photos.length - 1 });

    return this.setState(prevState => ({ currentIndex: prevState.currentIndex - 1 }));
  }

  loadPhotos() {
    const flickr = new Flickr(flickrConf.key);
    flickr.photos.search({
      tags: 'bskyb',
      extras: 'url_z',
      per_page: 20
    }).then(res => {
      this.setState({
        photos: res.body.photos.photo,
        currentIndex: 0
      });
    });
  }

  getCurrentPhoto() {
    if (this.state.currentIndex === -1) return;

    return this.state.photos[this.state.currentIndex];
  }

  renderCurrentPhoto() {
    const photo = this.getCurrentPhoto();
    if (!photo) return;

    const { currentIndex } = this.state;

    return (
      <img
        className="image"
        data-test-id={`carousel-image-${currentIndex}`}
        key={photo.id}
        src={photo.url_z}
      />
    );
  }

  render() {
    return (
      <div className="carousel">
        <div className="tag">#bskyb</div>
        <div className="carousel-image-box">
          <div className="slide" onClick={this.previousPhoto} />
          <div className="slide right" onClick={this.nextPhoto} />
          {this.renderCurrentPhoto()}
        </div>
      </div>
    );
  }
}
