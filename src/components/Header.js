import React, { Component } from 'react';

const Header = () => {
  return (
    <div className="header" data-test-id="header">
      <div className="logo" data-test-id="header-logo">
        <img src="http://www.stickpng.com/assets/images/5842ab6da6515b1e0ad75b0a.png" alt="Sky logo" />
      </div>
    </div>
  );
};

export default Header;
