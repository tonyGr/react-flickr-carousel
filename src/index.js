var express = require('express');
var path = require('path');

var app = express();

app.use(express.static(path.join(__dirname + '/../dist/static')));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/../dist/index.html'));
});

app.listen(8084);

console.log(
  '\n===========================' +
  '\nApp: http://localhost:8084/' +
  '\n==========================='
);
